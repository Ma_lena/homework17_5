#include <iostream>
#include <vector>
#include <math.h>
using namespace std;
class Vector
{
public:

	Vector() : x(5), y(4), z(3)
	{}
	
	double getLength(const double x, const double y, const double z)
	{
		return hypot(x, hypot(y, z));
	}
	
	void Show()
	{
		std::cout << x << " " << y << " " << z ;
	}
		
private:
	
	double x;
	double y;
	double z;
};


int main()
{
	Vector v;
	v.Show();
	Vector er;
	std::cout <<"\n" << er.getLength(5, 3, 8);
	
}
